<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $req)
    {
        $client = new Client();
        $url =  "http://127.0.0.1:8000/api/users";
        $response = $client->get($url);
        $data = $response->getBody()->getContents();
        $users = json_decode($data);
        return view("users")->with(['users' => $users->users]);
    }

    public function update(Request $req)
    {
        $client = new Client();
        $response = $client->request('POST', 'http://127.0.0.1:8000/api/users/update', [
            'form_params' => [
                'name' => $req->name ,
                'email' => $req->email ,
            ]
        ]);
    }
}
